close all;
clear all;
%% Inits
q_OA=[0.9588, 0, 0, 0.284]';
p_OA=[-0.0242,0.6822,0]';
q_OB=[0, 0, 0, 1]';
p_OB=[0.3884,0.5967,0.08]';
npts_total=3;
npts=1.5;
dt=0.01;
t=[0:npts*2/dt]*dt;

% If this is enabled(1), orientation is also controlled
quaternion_trejecory = 0;

%% 3-Polynomial Fit inits
if quaternion_trejecory==1
q_OM=[0.25,0,0,.4]';
% q_OM=[0,0,0,1]';
q_OM=q_OM/norm(q_OM);
end
p_OM=[0.275,0.6,0.08]';

if quaternion_trejecory==1
q_d1=zeros(4,length(t));
q_dot_d1=zeros(4,length(t));
q_ddot_d1=zeros(4,length(t));
end
p_d1=zeros(3,length(t));
p_dot_d1=zeros(3,length(t));
p_ddot_d1=zeros(3,length(t));

%% 3-Polynomial Fit no middle points
% for i=1:length(t)
%     if quaternion_trejecory==1
%     q_d1(:,i)=q_OA+3*(q_OB-q_OA)*(t(i)/(npts*2))^2-2*(q_OB-q_OA)*(t(i)/(npts*2))^3;
%     q_dot_d1(:,i)=6*(q_OB-q_OA)*t(i)/(npts*2)^2*(1-t(i)/(npts*2));
%     q_ddot_d1(:,i)=6*(q_OB-q_OA)/(npts*2)^2-12*(q_OB-q_OA)*t(i)/(npts*2)^3;
%     end
%     p_d1(:,i)=p_OA+3*(p_OB-p_OA)*(t(i)/(npts*2))^2-2*(p_OB-p_OA)*(t(i)/(npts*2))^3;
%     p_dot_d1(:,i)=6*(p_OB-p_OA)*t(i)/(npts*2)^2*(1-t(i)/(npts*2));
%     p_ddot_d1(:,i)=6*(p_OB-p_OA)/(npts*2)^2-12*(p_OB-p_OA)*t(i)/(npts*2)^3;
% end
%% 3-Polynomial Fit with one middle point
for i=1:npts/dt
    if quaternion_trejecory==1
    k_10=q_OA;
    k_11=0;
    k_12=(12*q_OM-3*q_OB-9*q_OA)/(4*(npts)^2);
    k_13=(-8*q_OM+3*q_OB+5*q_OA)/(4*(npts)^3);
    q_d1(:,i)=k_10+k_11*t(i)+k_12*t(i)^2+k_13*t(i)^3;
    q_dot_d1(:,i)=k_11+2*k_12*t(i)+3*k_13*t(i)^2;
    q_ddot_d1(:,i)=2*k_12+6*k_13*t(i);
    end
    p_10=p_OA;
    p_11=0;
    p_12=(12*p_OM-3*p_OB-9*p_OA)/(4*(npts)^2);
    p_13=(-8*p_OM+3*p_OB+5*p_OA)/(4*(npts)^3);
    p_d1(:,i)=p_10+p_11*t(i)+p_12*t(i)^2+p_13*t(i)^3;
    p_dot_d1(:,i)=p_11+2*p_12*t(i)+3*p_13*t(i)^2;
    p_ddot_d1(:,i)=2*p_12+6*p_13*t(i);
    if p_d1(1,i)>0.275 && p_d1(3,i)<0.08
        p_d1(:,i)
    end
%     q_d1(:,i)=q_d1(:,i)/norm(q_d1(:,i));

end

for i=1:npts/dt+1
    if quaternion_trejecory == 1
    k_20=q_OM;
    k_21=(3*q_OB-3*q_OA)/(4*npts);
    k_22=(-12*q_OM+6*q_OB+6*q_OA)/(4*(npts)^2);
    k_23=(8*q_OM-5*q_OB-3*q_OA)/(4*(npts)^3);
    q_d1(:,i+(length(t)+1)/2-1)=k_20+k_21*t(i)+k_22*t(i)^2+k_23*t(i)^3;
    q_dot_d1(:,i+(length(t)+1)/2-1)=k_21+2*k_22*t(i)+3*k_23*t(i)^2;
    q_ddot_d1(:,i+(length(t)+1)/2-1)=2*k_22+6*k_23*t(i);
    end
    p_20=p_OM;
    p_21=(3*p_OB-3*p_OA)/(4*npts);
    p_22=(-12*p_OM+6*p_OB+6*p_OA)/(4*(npts)^2);
    p_23=(8*p_OM-5*p_OB-3*p_OA)/(4*(npts)^3);
    p_d1(:,i+(length(t)+1)/2-1)=p_20+p_21*t(i)+p_22*t(i)^2+p_23*t(i)^3;
    p_dot_d1(:,i+(length(t)+1)/2-1)=p_21+2*p_22*t(i)+3*p_23*t(i)^2;
    p_ddot_d1(:,i+(length(t)+1)/2-1)=2*p_22+6*p_23*t(i);
    if p_d1(1,i)>0.275 && p_d1(3,i)<0.08
        p_d1(:,i)
    end
%     q_d1(:,i+(length(t)+1)/2-1)=q_d1(:,i+(length(t)+1)/2-1)/norm(q_d1(:,i+(length(t)+1)/2-1));

end
%% Linear function with Parabolic blends inits
v=[0.2;-0.05;0.05];
t_b=npts_total-abs((p_OB-p_OA)./v);
n_t_b=t_b/dt+1;
n_t_b=abs(int16(n_t_b));
a=v./t_b;

if quaternion_trejecory==1
q_d1_par=zeros(4,length(t));
q_dot_d1_par=zeros(4,length(t));
q_ddot_d1_par=zeros(4,length(t));
end
p_d1_par=zeros(3,length(t));
p_dot_d1_par=zeros(3,length(t));
p_ddot_d1_par=zeros(3,length(t));
% p_OA+a*t_b^2/2
% a*t_b^2/4

%% Linear function with Parabolic blends
for j=1:3
for i=1:n_t_b(j)
    p_10_par=p_OA(j);
    p_11_par=0;
    p_12_par=a(j)/2;
    p_d1_par(j,i)=p_10_par+p_11_par*t(i)+p_12_par*t(i)^2;
    p_dot_d1_par(j,i)=p_11_par+2*p_12_par*t(i);
    p_ddot_d1_par(j,i)=2*p_12_par;
end
for i=n_t_b(j):npts_total/dt+1-n_t_b(j)
    p_20_par=(p_OA(j)+p_OB(j)-v(j)*npts_total)/2;
    p_21_par=v(j);
    p_22_par=0;
    p_d1_par(j,i)=p_20_par+p_21_par*(t(i))+p_22_par*(t(i))^2;
    p_dot_d1_par(j,i)=p_21_par+2*p_22_par*t(i);
    p_ddot_d1_par(j,i)=2*p_22_par;
end
for i=npts_total/dt+1-n_t_b(j):npts_total/dt+1
    p_30_par=p_OB(j)-a(j)*npts_total^2/2;
    p_31_par=a(j)*npts_total;
    p_32_par=-a(j)/2;
    p_d1_par(j,i)=p_30_par+p_31_par*t(i)+p_32_par*t(i)^2;
    p_dot_d1_par(j,i)=p_31_par+2*p_32_par*t(i);
    p_ddot_d1_par(j,i)=2*p_32_par;
end
end

%% Linear function with Parabolic blends and inbetween positions
%This will only be implemented for the third axis
a_0_3=0.1485;
pos_3=0.1;
t_01_3=1.5;
t_b0_3=t_01_3-sqrt(t_01_3^2-2*(pos_3-p_OA(3))/a_0_3);
% v_01_3=(pos_3-q_OA(3)-a_0_3*(t_b0_3/2)^2/2)/(t_01_3-t_b0_3/2); %My take on velocity
v_01_3=(pos_3-q_OA(3))/(t_01_3-t_b0_3/2);
n_t_b_0=t_b0_3/dt+1;
n_t_b_0=abs(int16(n_t_b_0));

a_1_3=0.05;
t_12_3=npts_total-t_01_3;
t_b1_3=t_12_3-sqrt(t_12_3^2-2*(p_OB(3)-pos_3)/a_1_3);
% v_12_3=(p_OB(3)-pos_3-a_1_3*(t_b1_3/2)^2/2)/(t_12_3-t_b1_3/2);
v_12_3=(p_OB(3)-pos_3)/(t_12_3-t_b1_3/2);
n_t_b_1=t_b1_3/dt;
n_t_b_1=abs(int16(n_t_b_1));
% v_12_3*(npts_total-t(t_01_3/dt+1+n_t_b_1/2))+a_1_3*(t(n_t_b_1)^2/2);

for j=3:3
% for i=1:n_t_b_0/2
%     p_10_par=p_OA(j);
%     p_11_par=0;
%     p_12_par=a_0_3/2;
%     p_d1_par(j,i)=p_10_par+p_11_par*t(i)+p_12_par*t(i)^2;
%     p_dot_d1_par(j,i)=p_11_par+2*p_12_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_12_par;
% end
% for i=n_t_b_0/2:t_01_3/dt+1-n_t_b_1/2
%     p_20_par=(p_OA(j)+a_0_3/2*t(n_t_b_0/2)^2-v_01_3*t(n_t_b_0/2));
%     p_21_par=v_01_3;
%     p_22_par=0;
%     p_d1_par(j,i)=p_20_par+p_21_par*(t(i))+p_22_par*(t(i))^2;
%     p_dot_d1_par(j,i)=p_21_par+2*p_22_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_22_par;
% end
% for i=t_01_3/dt+1-n_t_b_1/2:t_01_3/dt+1
%     p_30_par=p_d1_par(j,t_01_3/dt+1-n_t_b_1/2)+t(t_01_3/dt+1-n_t_b_1/2)^2*a_1_3/2-v_01_3*t(t_01_3/dt+1-n_t_b_1/2);
%     p_31_par=v_01_3;
%     p_32_par=-a_1_3/2;
%     p_d1_par(j,i)=p_30_par+p_31_par*t(i)+p_32_par*t(i)^2;
%     p_dot_d1_par(j,i)=p_31_par+2*p_32_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_32_par;
% end
% for i=t_01_3/dt+1:t_01_3/dt+1+n_t_b_1/2
%     p_30_par=p_d1_par(j,t_01_3/dt+1)+t(t_01_3/dt+1)^2*a_1_3/2-v_01_3*t(t_01_3/dt+1);
%     p_31_par=+v_01_3;
%     p_32_par=-a_1_3/2;
%     p_d1_par(j,i)=p_30_par+p_31_par*t(i)+p_32_par*t(i)^2;
%     p_dot_d1_par(j,i)=p_31_par+2*p_32_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_32_par;
% end
% for i=t_01_3/dt+1+n_t_b_1/2:npts_total/dt+1
%     p_20_par=p_d1_par(j,t_01_3/dt+1+n_t_b_1/2)-v_12_3*t(t_01_3/dt+1+n_t_b_1/2);
%     p_21_par=v_12_3;
%     p_22_par=0;
%     p_d1_par(j,i)=p_20_par+p_21_par*(t(i))+p_22_par*(t(i))^2;
%     p_dot_d1_par(j,i)=p_21_par+2*p_22_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_22_par;
% end
% for i=npts_total/dt+1-n_t_b_2/2:npts_total/dt+1
%     p_30_par=p_d1_par(3,npts_total/dt+1-n_t_b_2/2)-v_23_3*t(npts_total/dt+1-n_t_b_2/2)+a_2_3/2*t(npts_total/dt+1-n_t_b_2/2)^2;
%     p_31_par=v_23_3;
%     p_32_par=-a_2_3/2;
%     p_d1_par(j,i)=p_30_par+p_31_par*t(i)+p_32_par*t(i)^2;
%     p_dot_d1_par(j,i)=p_31_par+2*p_32_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_32_par;
% end
end

% for j=3:3
% for i=1:n_t_b_0/2
%     p_10_par=p_OA(j);
%     p_11_par=0;
%     p_12_par=a_0_3/2;
%     p_d1_par(j,i)=p_10_par+p_11_par*t(i)+p_12_par*t(i)^2;
%     p_dot_d1_par(j,i)=p_11_par+2*p_12_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_12_par;
% end
% for i=n_t_b_0/2:t_01_3/dt+1-n_t_b_1/2
%     p_20_par=(p_OA(j)+a_0_3/2*t(n_t_b_0/2)^2-v_01_3*t(n_t_b_0/2));
%     p_21_par=v_01_3;
%     p_22_par=0;
%     p_d1_par(j,i)=p_20_par+p_21_par*(t(i))+p_22_par*(t(i))^2;
%     p_dot_d1_par(j,i)=p_21_par+2*p_22_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_22_par;
% end
% for i=t_01_3/dt+1-n_t_b_1/2:t_01_3/dt+1+n_t_b_1/2
%     p_30_par=p_d1_par(j,t_01_3/dt+1-n_t_b_1/2)-a_1_3*t(t_01_3/dt+1+n_t_b_1/2)*t(t_01_3/dt+1-n_t_b_1/2)+t(t_01_3/dt+1-n_t_b_1/2)^2*a_1_3/2;
%     p_31_par=a_1_3*t(t_01_3/dt+1+n_t_b_1/2);
%     p_32_par=-a_1_3/2;
%     p_d1_par(j,i)=p_30_par+p_31_par*t(i)+p_32_par*t(i)^2;
%     p_dot_d1_par(j,i)=p_31_par+2*p_32_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_32_par;
% end
% for i=t_01_3/dt+1+n_t_b_1/2:npts_total/dt+1
%     p_30_par=p_d1_par(3,t_01_3/dt+1+n_t_b_1/2)-v_12_3*t(t_01_3/dt+1+n_t_b_1/2);
%     p_31_par=v_12_3;
%     p_32_par=0;
%     p_d1_par(j,i)=p_30_par+p_31_par*t(i)+p_32_par*t(i)^2;
%     p_dot_d1_par(j,i)=p_31_par+2*p_32_par*t(i);
%     p_ddot_d1_par(j,i)=2*p_32_par;
% end
% end

%% Plots

if quaternion_trejecory == 1
figure('Position',[500 0 1420 1080]);
for i=1:4
subplot(4,3,3*(i-1)+1);
plot(t,q_d1(i,:), 'LineWidth',2.0, 'Color','blue');
ylabel(['$q_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Simulation', 'interpreter','latex', 'fontsize',17);end
end
for i=1:4
subplot(4,3,3*(i-1)+2);
plot(t,q_dot_d1(i,:), 'LineWidth',2.0, 'Color','blue');
ylabel(['$\dot{q}_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Velocity', 'interpreter','latex', 'fontsize',17);end
end

for i=1:4
subplot(4,3,3*(i-1)+3);
plot(t,q_ddot_d1(i,:), 'LineWidth',2.0, 'Color','blue');
ylabel(['$\ddot{q}_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Acceleration', 'interpreter','latex', 'fontsize',17);end
end
end

k=[];
l=[];
for j=1:length(t)
if p_d1(1,j)>0.274 && p_d1(3,j)<0.08
k=[k p_d1(:,j)];
l=[l j];
end
end
l=l*dt;

h=figure('Position',[500 0 1420 1080]);
for i=1:3
subplot(3,3,3*(i-1)+1);
hold on;
plot(t,p_d1(i,:), 'LineWidth',2.0, 'Color','blue');
if isempty(l)~=1
plot(l,k(i,:), 'LineWidth',2.0, 'Color','red');
end
hold off;
ylabel(['$p_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Position', 'interpreter','latex', 'fontsize',17);end
hold off;
end
for i=1:3
subplot(3,3,3*(i-1)+2);
plot(t,p_dot_d1(i,:), 'LineWidth',2.0, 'Color','blue');
ylabel(['$\dot{p}_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Velocity', 'interpreter','latex', 'fontsize',17);end
end

for i=1:3
subplot(3,3,3*(i-1)+3);
plot(t,p_ddot_d1(i,:), 'LineWidth',2.0, 'Color','blue');
ylabel(['$\ddot{p}_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Acceleration', 'interpreter','latex', 'fontsize',17);end
end

k=[];
l=[];
for j=1:length(t)
if p_d1_par(1,j)>0.274 && p_d1_par(3,j)<0.08
k=[k p_d1_par(:,j)];
l=[l j];
end
end
l=l*dt;

o=figure('Position',[500 0 1420 1080]);
for i=1:3
subplot(3,3,3*(i-1)+1);
hold on;
plot(t,p_d1_par(i,:), 'LineWidth',2.0, 'Color','blue');
if isempty(l)~=1
plot(l,k(i,:), 'LineWidth',2.0, 'Color','red');
end
hold off;
ylabel(['$p_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Position', 'interpreter','latex', 'fontsize',17);end
hold off;
end
for i=1:3
subplot(3,3,3*(i-1)+2);
plot(t,p_dot_d1_par(i,:), 'LineWidth',2.0, 'Color','blue');
ylabel(['$\dot{p}_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Velocity', 'interpreter','latex', 'fontsize',17);end
end

for i=1:3
subplot(3,3,3*(i-1)+3);
plot(t,p_ddot_d1_par(i,:), 'LineWidth',2.0, 'Color','blue');
ylabel(['$\ddot{p}_' num2str(i) '$'], 'interpreter','latex', 'fontsize',17);
if(i==1)title('Acceleration', 'interpreter','latex', 'fontsize',17);end
end
%% Print
% set(h,'Units','Inches');
% pos = get(h,'Position');
% set(h,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
% print(h,'1_a_init','-dpdf','-r0')
% lspb

%% Tranimate
T=zeros(4,4,length(p_d1));
figure();

[X_c,Y_c,Z_c]=cylinder(2.5);
Z_c=Z_c*10;
X_c = bsxfun(@plus, X_c, [-2.42;-2.42]);
Y_c = bsxfun(@plus, Y_c, [68.22;68.22]);
% mesh(X_c,Y_c,Z_c)
surf(X_c,Y_c,Z_c);
hold on;
P = [59.67,59.67,4] ;   % you center point 
L = [59.34,59.34,8] ;  % your cube dimensions 
O = P-L/2 ;       % Get the origin of cube so that P is at center 
plotcube(L,O,.8,[1 0 0]);   % use function plotcube 
plot3(p_OB(1)*100,p_OB(2)*100,p_OB(3)*100,'*k')
for i=1:length(p_d1)
    T(:,:,i)=transl(p_d1(1,i), p_d1(2,i), p_d1(3,i));
end
p=zeros(4,4,5*length(p_d1));
for i=2:length(p_d1)
p(:,:,5*(i-2)+1:5*(i-2)+5)=ctraj(T(:,:,i-1),T(:,:,i),5);
end
p(1:3,4,:)=p(1:3,4,:)*100;
tranimate(p);
hold off;
axis([-20 59.67+30 0 59.34+30 0 18])
view(20,60)


T=zeros(4,4,length(p_d1_par));
figure();

[X_c,Y_c,Z_c]=cylinder(2.5);
Z_c=Z_c*10;
X_c = bsxfun(@plus, X_c, [-2.42;-2.42]);
Y_c = bsxfun(@plus, Y_c, [68.22;68.22]);
% mesh(X_c,Y_c,Z_c)
surf(X_c,Y_c,Z_c);
hold on;
P = [59.67,59.67,4] ;   % you center point 
L = [59.34,59.34,8] ;  % your cube dimensions 
O = P-L/2 ;       % Get the origin of cube so that P is at center 
plotcube(L,O,.8,[1 0 0]);   % use function plotcube 
plot3(p_OB(1)*100,p_OB(2)*100,p_OB(3)*100,'*k')
for i=1:length(p_d1_par)
    T(:,:,i)=transl(p_d1_par(1,i), p_d1_par(2,i), p_d1_par(3,i));
end
p=zeros(4,4,5*length(p_d1_par));
for i=2:length(p_d1_par)
p(:,:,5*(i-2)+1:5*(i-2)+5)=ctraj(T(:,:,i-1),T(:,:,i),5);
end
p(1:3,4,:)=p(1:3,4,:)*100;
tranimate(p)
hold off;
axis([-20 59.67+30 0 59.34+30 0 18])
view(20,60)
